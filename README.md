# KL25Z platform library
This package provides all low-level libraries and startup routine for the KL25Z
board.

Note: Still highly experimental and untested.

# Requirements

* conan
* ninja-build
* arm-none-eabi-gcc

# Usage

Import `platform-kl25z` with conan.

Then, create a main.cpp file to make a blinky led

```
#include "/platform-kl25z/MKL25Z4.h"

int main(void)
{
  int counter = 0;
  /* Turn on clock to PortB module */
  SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;

  /* Set the PTB18 pin multiplexer to GPIO mode */
  PORTB_PCR18 = PORT_PCR_MUX(1);

  /* Set the initial output state to high */
  GPIOB_PSOR |= 1 << 18;

  /* Set the pins direction to output */
  GPIOB_PDDR |= 1 << 18;

  for(;;)
  {
    counter++;
    delay();
    if((counter == 524288)
    {
      GPIOB_PTOR = 1 << 18;
    }
  }
  return 0;
}
```

You will need this `build.ninja` script to produce the binary executable that can be
drad'n'drop into the KL25Z board.

```
cflags = -Wall -Icore -mcpu=cortex-m0plus --specs=nosys.specs -mthumb -ffunction-sections -fdata-sections -fmessage-length=0 -mfloat-abi=soft
libs = -I./deps
rule cpp
  command = arm-none-eabi-g++ $cflags $libs -DCPU_MKL25Z128VFM4=1 -c $in -o $out

rule cc
  command = arm-none-eabi-gcc $cflags $libs -DCPU_MKL25Z128VFM4=1 -c $in -o $out

rule link
  command = arm-none-eabi-gcc $cflags -T platform/MKL25Z4/linker/gcc/MKL25Z128xxx4_flash.ld -o $out $in -L ./build -lplatform-kl25z

rule tobinary
  command = arm-none-eabi-objcopy -O binary $in $out

# 1 - Build main file
build build/main.o : cpp main/main.cpp

# 2 - Link all in a .elf file
build build/main.elf : link build/main.o

# 3 - Convert the .elf into a raw binary file that can be drag'n'drop on the board (with OpenSDA mass-storage bootloader)
build build/main.bin : tobinary build/main.elf

```

To execute the script, then run
```
ninja
```
