from conans import ConanFile

class PlatformKL25Z(ConanFile):
    name = "platform-kl25z"
    version = "0.1.0"
    url = "https://gitlab.com/Overdrivr/platform-KL25Z"
    settings = "build_type", "arch"
    exports = "*"

    def build(self):
        self.run("ninja")

    def package(self):
        self.copy("*.h", dst="include", src="platform/MKL25Z4/include")
        self.copy("*.h", dst="include", src="platform/")
        self.copy("*.a", dst="lib", src="build")
        self.copy("build.ninja", dst=".", src=".")

    def package_info(self):
        self.cpp_info.libs = ["platform-kl25z"]
